#!/bin/sh
# Copyright 2019 Collabora Ltd.
# SPDX-License-Identifier: Zlib
# (see "zlib/libpng" in debian/copyright)

set -eux

if [ -n "${AUTOPKGTEST_ARTIFACTS-}" ]; then
	WORKDIR="$AUTOPKGTEST_ARTIFACTS"
else
	WORKDIR="$(mktemp -d)"
	trap 'cd /; rm -fr "$WORKDIR"' 0 INT QUIT ABRT PIPE TERM
fi

if [ -n "${DEB_HOST_GNU_TYPE:-}" ]; then
    CROSS_COMPILE="$DEB_HOST_GNU_TYPE-"
else
    CROSS_COMPILE=
fi

cp showinterfaces.c "$WORKDIR"
mkdir "$WORKDIR/cmake"
cp ./cmake/test/CMakeLists.txt "$WORKDIR/cmake"
cp ./cmake/test/main.c "$WORKDIR/cmake"

cd "$WORKDIR"

# Deliberately word-splitting pkgconf's output:
# shellcheck disable=SC2046
"${CROSS_COMPILE}gcc" -oshowinterfaces showinterfaces.c $("${CROSS_COMPILE}pkgconf" --cflags --libs SDL2_net)
set -- xvfb-run -a -s '-screen 0 1280x1024x24 -noreset'
"$@" ./showinterfaces

if [ -z "${DEB_HOST_GNU_TYPE:-}" ]; then
    mkdir cmake/_build
    ( cd cmake/_build; cmake -GNinja -DTEST_SHARED=ON -DTEST_STATIC=ON .. )
    ninja -C cmake/_build
    ./cmake/_build/main_shared
    ./cmake/_build/main_static
fi
